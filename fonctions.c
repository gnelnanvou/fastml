#include "fonctions.h"


matrice logisticRegressionAlgorithm(double alpha, double seuil, int n_epoch, char * filename){
	matrice X,Y,aux;
	readDatas(&aux,&Y,filename,12);
	X = ones(aux.n,1);
	concatColoumn(&X,aux);
	freeMatrice(&aux);
	for (int i = 0; i < Y.n; i++)
	{
		Y.contenu[i][0] = (Y.contenu[i][0] <=5)?0:1;
	}

	top_(&w_time_par1,&tz);
	matrice w = SGD( X, Y,cross_entropy_binaryclass,cross_entropy_binaryclass_gradient, alpha, seuil, n_epoch);
	top_(&w_time_par2,&tz);
	work_time_par = cpu_time_(w_time_par1, w_time_par2);
	//printf("\n \ttemps d'execution Regression Logistique avec %d threads = %lld.%03lldms\n\n\n\n",N_THREADS,work_time_par/1000, work_time_par%1000);
	FILE *fi = fopen("time/fastml/logistic.txt","a");
    fprintf(fi, "%d %lld.%03lld\n",N_THREADS, work_time_par/1000, work_time_par%1000);
    fclose(fi);
	freeMatrice(&X);
	freeMatrice(&Y);
	return w;
}

matrice linearRegressionAlgorithm(double alpha, double seuil, int n_epoch, char * filename){
	matrice X,Y,aux;
	readDatas(&aux,&Y,filename,12);
	X = ones(aux.n,1);
	concatColoumn(&X,aux);
	freeMatrice(&aux);

	top_(&w_time_par1,&tz);
	matrice w = SGD( X, Y,means_square_error,means_square_error_gradient, alpha, seuil, n_epoch);
	top_(&w_time_par2,&tz);

	work_time_par = cpu_time_(w_time_par1, w_time_par2);
	//printf("\n \ttemps d'execution Regression Lineaire avec %d threads = %lld.%03lldms\n\n\n\n",N_THREADS,work_time_par/1000, work_time_par%1000);
	FILE *fi = fopen("time/fastml/linear.txt","a");
    fprintf(fi, "%d %lld.%03lld\n",N_THREADS, work_time_par/1000, work_time_par%1000);
    fclose(fi);
	freeMatrice(&X);
	freeMatrice(&Y);

	return w;
}
double modele_regression_lineaire(matrice w,matrice xi){	
	matrice p = product(xi,w);
	double d = p.contenu[0][0];
	freeMatrice(&p);
	return d;
}

double modele_regression_logistique(matrice w,matrice xi){
	// if(sigmoid(product(xi,w).contenu[0][0])>0.5) return 1.0;
	// return 0.0;
	matrice p = product(xi,w);
	double d = sigmoid(p.contenu[0][0]);
	freeMatrice(&p);
	return d;
}


matrice * kmeans(int k,int n_epoch,double seuil,char * filename){
	matrice X;
	readMatrice(&X,filename);

	matrice * clusters = (matrice *)malloc(k * sizeof(matrice));
	matrice * old_clusters;
	initialize_centroid(clusters, X, k);
	int iter = 1;
    double error = seuil;

    top_(&w_time_par1,&tz);
	while(n_epoch >= iter && error >= seuil){
		matrice index = determineBestCluster(X, clusters, k);

		old_clusters = clusters;

		clusters = update_centroid(X, index, k);

		freeMatrice(&index);

	    error = calcul_erreur(old_clusters,clusters, k);

	    for(int i =0; i<k; i++){
			freeMatrice(&old_clusters[i]);
    	}
    	free(old_clusters);

	    //printf("Epoch %d : error  = %4.10f\n",iter,error);
	    iter++;
	}
	top_(&w_time_par2,&tz);

	work_time_par = cpu_time_(w_time_par1, w_time_par2);
	//printf("\n \ttemps d'execution Kmeans avec %d threads = %lld.%03lldms\n\n\n\n",N_THREADS,work_time_par/1000, work_time_par%1000);
	FILE *fi = fopen("time/fastml/sortie_Kmeans_8_clusters.txt","a");
    fprintf(fi, "%d %lld.%03lld\n",N_THREADS, work_time_par/1000, work_time_par%1000);
    fclose(fi);

	return clusters;

}


matrice * update_centroid(matrice X, matrice indexe,int k){
    matrice aux;
    int * n_element_par_cluster = (int*) malloc (k*sizeof(int));
    matrice * somme = (matrice *)malloc(k * sizeof(matrice));
    for(int i =0; i<k; i++){
        somme[i] = zeros(1,X.m);
        n_element_par_cluster[i] = 0;
    }

    for(int i =0; i<indexe.n; i++){
    	extractLine(&aux,X,i);
    	for (int j = 0; j < k; j++)
    	{
    		if(indexe.contenu[i][0] == (long double)j){
    			som_interne(&somme[j],aux);
    			n_element_par_cluster[j]++;
    			break;
    		}
    	}
    	freeMatrice(&aux);
    }

    for(int i =0; i<k; i++){
       produit_scalaire_matrice((long double)1/(long double)n_element_par_cluster[i],&somme[i]);
    }
    free(n_element_par_cluster);
    return somme;
}

double calcul_erreur(matrice * old_clusters, matrice * clusters, int k){
	double e = 0.0;
	for(int i =0; i<k; i++){
		e += distance(old_clusters[i],clusters[i]);
    }

    return e/(double)k;
}
