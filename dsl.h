#ifndef _DSL
#define _DSL

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <pthread.h>
#include "time_manager.h"
#include "matrice.h"


   typedef struct {
      int index;
      int debut;
      int fin;
      double alpha;
      matrice gradient_local;
      matrice (*gradient)(matrice,matrice,double);
      double (*fonction_cout)(matrice,matrice,double);
      double cout;
      int * ordre;
      matrice w;
      matrice X;
      matrice Y;
   }Argument_SGD;

   typedef struct {
      int index;
      int debut;
      int fin;
      matrice X;
      int k;
      matrice * clusters;
      int * bon_cluster;
   }Argument_procheCluster;

   int N_THREADS;

   unsigned long long work_time_par;
timeval_t w_time_par1, w_time_par2, tz;
matrice w;


matrice SGD(matrice X, matrice Y,double (*fonction_cout)(matrice,matrice,double),matrice (*gradient)(matrice,matrice,double),double alpha,double seuil,int n_epoch);

double means_square_error(matrice w,matrice xi,double yi);
double means_error(matrice w,matrice xi,double yi);
double means_absolute_error(matrice w,matrice xi,double yi);
double cross_entropy_binaryclass(matrice w,matrice xi,double yi);
double cross_entropy_multiclass(matrice w,matrice xi,double yi);

matrice means_square_error_gradient(matrice w,matrice xi,double yi);
matrice means_error_gradient(matrice w,matrice xi,double yi);
matrice means_absolute_error_gradient(matrice w,matrice xi,double yi);
matrice cross_entropy_binaryclass_gradient(matrice w,matrice xi,double yi);
matrice cross_entropy_multiclass_gradient(matrice w,matrice xi,double yi);

long double sigmoid(double x);

int * range(int size); // array 0 to size-1

void shuffle(int *array, int n);

int select_random_number(int min, int max);
void * sgd_local(void *a);


     
      
/********************** fonctions propres à l'algorithme à implémenter **************************/

  //** To initialize centroids of theese clusters 
void initialize_centroid(matrice * list_clusters,matrice X,int k); 
double cluster_proche(int j);
matrice determineBestCluster(matrice X, matrice * list_clusters,int k); // retourne pour chaque point, l'index du cluster auquel il est plus proche
void * proche_distance_local(void *a);



#endif