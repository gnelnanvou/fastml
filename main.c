/*** 06-05-2021 
	@ autor : NANVOU TSOPGNY Nel Gerbault
	Dans le cadre de mon mémoire de recherhe
	Université Yaoundé 1

	"DSL pour la Parallélisation des Algorithmes du Machine Learning"
 ***/

#include "fonctions.h"


int main(int argc, char const *argv[])
{
	double alpha = 0.00001;
	double seuil = 0.01;
	int n_epoch = 1000;
	char * filename;
	int choix;

	
	
	if(argc == 4){
		N_THREADS = atoi(argv[1]);
		filename = argv[2];
		choix = atoi(argv[3]);
	} 
	else {
		printf("incorrect format : Expected 'main NbThread data_filename choix'\n");
		exit(-1);
	}



if(choix == 1){
	/* %%%%%%%%%%%%%%%% REGRESSION LINEAIRE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */

	matrice w1 = linearRegressionAlgorithm(alpha, seuil, n_epoch, filename);
	print(w1);
}
else if(choix == 2){
	/* %%%%%%%%%%%%%%%% Regression Logistique (deux classes 0 et 1) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */

	alpha = 0.01;
	matrice w2 = logisticRegressionAlgorithm(alpha, seuil, n_epoch, filename);
	print(w2);

}
else if(choix == 3){
		/* %%%%%%%%%%%%%%%% kmeans %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% */
	 int k = 8;
	 seuil = 0.0001;

	 matrice * centroid = kmeans(k, n_epoch, seuil, filename);

	 for (int i = 0; i < k; i++)
	 {
	 	print(centroid[i]);
	 }
}

	
	return EXIT_SUCCESS;
}
