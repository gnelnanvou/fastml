#ifndef _FONCTIONS
#define _FONCTIONS

#include "dsl.h"


matrice logisticRegressionAlgorithm(double alpha, double seuil, int n_epoch, char * filename);

matrice linearRegressionAlgorithm(double alpha, double seuil, int n_epoch, char * filename);
	
double modele_regression_logistique(matrice w,matrice xi);


double modele_regression_lineaire(matrice w,matrice xi);

matrice * kmeans(int k,int n_iter,double seuil,char * filename);
matrice * update_centroid(matrice X, matrice indexe,int k);
double calcul_erreur(matrice * old_clusters, matrice * clusters, int k);

#endif