#include "dsl.h"
#include "matrice.h"


matrice determineBestCluster(matrice X, matrice * clusters, int k){
     
    matrice result = init_value(X.n,1,-1);

    pthread_t threads[N_THREADS];
    Argument_procheCluster argument[N_THREADS];
    

    for( int i = 0 ; i < N_THREADS; i++){
        Argument_procheCluster *ar = &argument[i];
        ar->index = i;
        ar->X = X;
        ar->k = k;
        ar->clusters = clusters;
        int fraction = (int) (X.n / N_THREADS);
        ar->debut = i*fraction;
        ar->fin = i == (N_THREADS - 1) ? X.n : ar->debut + fraction; 
        pthread_create(&threads[i], NULL, proche_distance_local, (void*)ar);
    }
    for( int i = 0 ; i < N_THREADS; i++){
        pthread_join(threads[i], NULL);
    }


    int t = 0;
    for (int i = 0; i<N_THREADS; i++){
        int nombre = argument[i].fin - argument[i].debut;
        for(int j = 0;j<nombre;j++){
            result.contenu[t][0] = argument[i].bon_cluster[j];
            t++;
        }
    }
      
   return result;

}

void * proche_distance_local(void *a){
    Argument_procheCluster * ar = (Argument_procheCluster *) a ;
    int debut = ar->debut; 
    int fin = ar->fin;
    int k = ar->k;
    int nombre = fin - debut;
    // int nombre = fin - debut + 1;
    ar->bon_cluster = (int *)malloc(nombre*sizeof(int));
    matrice X = ar->X;
    int index = ar->index;
    int t = 0,index_min = 0;
    double v,d;
    matrice aux;
    for (int i = debut; i < fin; i++)
    {
        extractLine(&aux,X,i);
        d = INFINITY;
        for (int j = 0; j < k; j++)
        {
            v = distance(ar->clusters[j],aux);
            if(v<d){
                d = v;
                index_min = j;
            }

        }
        ar->bon_cluster[t] = index_min;
        freeMatrice(&aux);
        t++;

    } 

}
void initialize_centroid(matrice * list_clusters,matrice X, int k){
    srand(time(0));
    matrice deja = init_value(1,k,-1);
    for(int i = 0; i<k; i++){
        matrice aux;
        int rd,test = 0,j = 0;
        rd = select_random_number(0,X.n-1);
        while(j < i){
            if(deja.contenu[0][j] != rd) j++;
            else {
                rd = select_random_number(0,X.n-1);
                j = 0;
            }
        }
        deja.contenu[0][i] = rd;
        extractLine(&aux,X,rd);
        copy(&list_clusters[i],aux);
        freeMatrice(&aux);
    }
    freeMatrice(&deja);
}
/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%*/

/*Descente de gradient stochastique*/

matrice SGD(matrice X, matrice Y,double (*fonction_cout)(matrice,matrice,double),matrice (*gradient)(matrice,matrice,double),double alpha,double seuil,int n_epoch){
    int iter = 1;
    double error = seuil;
    double * resultFinal;
    int debut,fin;
    matrice w = zeros(X.m,1);
    int * ordre = range(X.n);
    while(n_epoch >= iter && error >= seuil){
        //printf("\nepoch %d : \n",iter);

        pthread_t threads[N_THREADS];
        Argument_SGD argument[N_THREADS];
        
        shuffle(ordre,X.n);

        for( int i = 0 ; i < N_THREADS; i++){
            Argument_SGD *ar = &argument[i];
            ar->index = i;
            ar->alpha = alpha;
            ar->X = X;
            ar->Y = Y;
            copy(&ar->w,w);
            
            ar->ordre = ordre;
            ar->gradient = gradient;
            ar->fonction_cout = fonction_cout;
            int fraction = (int) (X.n / N_THREADS);
            ar->debut = i*fraction;
            ar->fin = i == (N_THREADS - 1) ? X.n : ar->debut + fraction; 
            pthread_create(&threads[i], NULL, sgd_local, (void*)ar);
        }
        for( int i = 0 ; i < N_THREADS; i++){
            pthread_join(threads[i], NULL);
        }
         freeMatrice(&w);
         w = zeros(X.m,1);
         double cout = 0.0;
        for( int k = 0 ; k < N_THREADS; k++){
            w = sum (w,argument[k].gradient_local);
            cout += argument[k].cout;
            freeMatrice(&argument[k].gradient_local);
        }
        cout/=X.n;
        produit_scalaire_matrice((double)1/(double)N_THREADS,&w);
         //print(w);
         //printf("Erreur  = %lf \n",cout);

         error = cout;
         iter++;
  
    }
    free(ordre);
    return w;
}

/* fonction exécutée par chaque thread pour la descente de gradient */

void * sgd_local(void *a){
    Argument_SGD * ar = (Argument_SGD *) a ;
    int debut = ar->debut; 
    int fin = ar->fin;

    int nombre = fin - debut;
    // int nombre = fin - debut + 1;

    double alpha = ar->alpha;
    matrice wk;
    copy(&wk,ar->w);
   freeMatrice(&ar->w);
    matrice X = ar->X;
    matrice Y = ar->Y;
    int * ordre = ar->ordre;

    int index = ar->index;

    ar->cout = 0;
    for (int i = debut; i < fin; i++)
    {
        matrice line;
        extractLine(&line,X,ordre[i]);
        matrice label;
        extractLine(&label,Y,ordre[i]);
        double d = ar->fonction_cout(wk,line,label.contenu[0][0]);
        matrice gradient = ar->gradient(wk,line,label.contenu[0][0]);
        ar->cout += d;
        produit_scalaire_matrice((long double)alpha,&gradient);
        matrice t = transpose(gradient);
        freeMatrice(&gradient);
        sous_interne(&wk,t);
        freeMatrice(&t);
        freeMatrice(&line);
        freeMatrice(&label);

    }   

    ar->gradient_local = wk;

return NULL;
}



matrice means_square_error_gradient(matrice w,matrice xi,double yi){
    matrice p = product(xi,w);
    double scalaire = p.contenu[0][0] - yi;
    freeMatrice(&p);
    matrice r;
    copy(&r,xi);
    produit_scalaire_matrice(scalaire,&r);
    return r;
}
matrice means_error_gradient(matrice w,matrice xi,double yi){
    // a revoir
    matrice r;
    copy(&r,xi);
    produit_scalaire_matrice(-1,&r);
    return r;
}
matrice means_absolute_error_gradient(matrice w,matrice xi,double yi){
    matrice p = product(xi,w);
    double scalaire = p.contenu[0][0] - yi;
    freeMatrice(&p);
    if (scalaire>0) return ones(1,xi.m);
    return init_value(1,xi.m,-1);

    // a revoir 
}
 /* fonction cout pour la classification binaire : les classes doivent être 0 et 1 */

matrice cross_entropy_binaryclass_gradient(matrice w,matrice xi,double yi){
    matrice p = product(xi,w);
    long double predict = sigmoid(p.contenu[0][0]);
    freeMatrice(&p);
    predict = (long double)yi - predict;
    matrice r;
    copy(&r,xi);
    produit_scalaire_matrice(-1*predict,&r);
    return r;


}
matrice cross_entropy_multiclass_gradient(matrice w,matrice xi,double yi);

long double sigmoid(double x){
    long double r = (long double)1.0/(long double)(1.0 + exp(-1*x));
   return (r>0.999)?0.999:(r<0.001)?0.001:r;
}

double means_square_error(matrice w,matrice xi,double yi){
    matrice p = product(xi,w);
    double scalaire = p.contenu[0][0] - yi;
    freeMatrice(&p);
    return 0.5*scalaire*scalaire;
}
double means_error(matrice w,matrice xi,double yi){
    matrice p = product(xi,w);
    double scalaire = p.contenu[0][0] - yi;
    freeMatrice(&p);
    return 0.5*scalaire;
}
double means_absolute_error(matrice w,matrice xi,double yi){
    matrice p = product(xi,w);
    double scalaire = p.contenu[0][0] - yi;
    freeMatrice(&p);
    return 0.5*abs(scalaire);
}
double cross_entropy_binaryclass(matrice w,matrice xi,double yi){
    matrice p = product(xi,w);
    long double predict = sigmoid(p.contenu[0][0]);
    freeMatrice(&p);
    return -1*(yi*log(predict)+(1-yi)*log(1-predict));
}
double cross_entropy_multiclass(matrice w,matrice xi,double yi){

}

int * range(int size){
    int * r =(int *) malloc(size * sizeof(int));
    for (int i = 0; i < size; i++)
     {
         r[i] = i;
     } 
     return r;
}
void shuffle(int *array, int n)
{
    if (n > 1) 
    {
        for (int i = 0; i < n; i++) 
        {
          int j = select_random_number(i,n-1);
          int t = array[j];
          array[j] = array[i];
          array[i] = t;
        }
    }
}
// min et max inclus
int select_random_number(int min, int max){
    srand(time(0));
   return rand() %(max-min+1) + min ; 
}

