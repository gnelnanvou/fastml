main:main.c  matrice.c matrice.h dsl.c dsl.h fonctions.c fonctions.h time_manager.c time_manager.h
	gcc -o  main main.c dsl.c matrice.c fonctions.c time_manager.c -lm -lpthread
run:
	./main 1

clean:
	rm main
run_valgrind:main
	valgrind  --tool=memcheck --leak-check=yes --leak-resolution=low --show-reachable=yes  ./main 1
